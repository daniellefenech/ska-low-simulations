This Matlab directory contains the StatSim package, which provides
functions to simulate the SKA1-LOW station response and generate
SKA1-LOW simulated data in Matlab.

The StatSim package has been organised as a Matlab toolbox, so to use
it, just download the StatSim directory and add it to your Matlab
path. Typing "help StatSim" should get you started with using the
package.

For questions or comments, please contact Stefan WIjnholds
(wijnholds-at-astron-dot-nl),
