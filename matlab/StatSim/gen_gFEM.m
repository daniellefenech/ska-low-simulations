function [gFEMx, gFEMy] = gen_gFEM(f, epsmag_std, epsph_std, TFEM)

% [gFEMx, gFEMy] = gen_gFEM(f, epsmag_std, epsph_std, TFEM)
%
% Function to generate gains of the FEM module of the SKA-LOW antennas
% based on the physical model:
%
% gFEM = g0 * 10^(-beta * (TLNA - Tref) / 20)
%
% For more details on this model and the underlying assumptions, the reader
% is referred to
% https://confluence.skatelescope.org/display/TDT/Signal+Chain+Behavioural+Modeling
%
% As beta turns out to be temperature-dependent as well, the current
% implementation uses table interpolation.
%
% Arguments:
% f          : Nf-element column vector with frequencies at which to
%              generate gain values (in Hz)
% epsmag_std : RMS gain magnitude variation between receive paths (in dB)
% epsph_std  : RMS gain phase variations between receive paths (in deg)
% TFEM       : Nant x Nt matrix with FEM temperatures for each receive
%              path at each time instance (in K)
%
% Return value
% gFEMx      : Nant x Nf x Nt tensor containing generated LNA gain values
%              (complex valued, voltage domain) for x-polarisation
% gFEMy      : Nant x Nf x Nt tensor containing generated LNA gain values
%              (complex valued, voltage domain) for y-polarisation
%
% Stefan J. Wijnholds, 30 July 2020
% Last modified on 6 August 2020 by Stefan J. Wijnholds

% determine simulation size
Nf = length(f);
Nt = size(TFEM, 2);
Nant = size(TFEM, 1);

% read measured data on nominal gain
datax = xlsread('g0FEM_xpol.xls');
datay = xlsread('g0FEM_ypol.xls');

% construct interpolation table
freq = datax(:, 1);             % measurement frequencies in Hz
Tmeas = [-10 20 50 70 80] + 273;% measurement temperatures in K
g0magx = datax(1:end, 3:7);     % measured gain magnitudes x-pol (dB)
g0phx = datax(1:end, 8:12);     % measured gain phases x-pol (deg)
g0magy = datay(1:end, 3:7);     % measured gain magnitudes y-pol (dB)
g0phy = datay(1:end, 8:12);     % measured gain phases y-pol (dB)

% unwrap measured phases along frequency axis
% note: phases converted to radians in the process
g0phx = unwrap(g0phx * pi / 180, [], 2);
g0phy = unwrap(g0phy * pi / 180, [], 2);

% determine nominal gains
g0FEMx = zeros(Nant, Nf, Nt);
g0FEMy = zeros(Nant, Nf, Nt);
for elemidx = 1:Nant
    g0FEMx_mag = interp2(freq, Tmeas, g0magx.', f, TFEM(elemidx, :));
    g0FEMx_ph = interp2(freq, Tmeas, g0phx.', f, TFEM(elemidx, :));
    g0FEMy_mag = interp2(freq, Tmeas, g0magy.', f, TFEM(elemidx, :));
    g0FEMy_ph = interp2(freq, Tmeas, g0phy.', f, TFEM(elemidx, :));
    % conversion to complex gain values
    g0FEMx(elemidx, :, :) = 10.^(g0FEMx_mag.'/20) .* exp(1i * g0FEMx_ph.');
    g0FEMy(elemidx, :, :) = 10.^(g0FEMy_mag.'/20) .* exp(1i * g0FEMy_ph.');
end

% apply production tolerances, x-polarisation
% modelled as constant offsets across frequency for each element, offsets
% assumed to have Gaussian distribution across elements
epsmag = epsmag_std * randn(Nant, 1);
epsph = epsph_std * randn(Nant, 1);
gerr = 10.^(epsmag/20) .* exp(1i * epsph * pi / 180);

gFEMx = zeros(Nant, Nf, Nt);
for fidx = 1:Nf
    gFEMx(:, fidx, :) = diag(gerr) * squeeze(g0FEMx(:, fidx, :));
end

% apply production tolerances, y-polarisation
epsmag = epsmag_std * randn(Nant, 1);
epsph = epsph_std * randn(Nant, 1);
gerr = 10.^(epsmag/20) .* exp(1i * epsph * pi / 180);

gFEMy = zeros(Nant, Nf, Nt);
for fidx = 1:Nf
    gFEMy(:, fidx, :) = diag(gerr) * squeeze(g0FEMy(:, fidx, :));
end
