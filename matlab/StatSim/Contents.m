% SKA-LOW Station Simulation Package
%
% This Matlab package provides functions to simulate a realistic SKA1-LOW
% station (beam) response and SKA1-LOW station level data products. The
% instrumental respnse is based on validated EM simulations of the antenna
% response combined with a receive path gain model based on physical
% principles. The model for the receive path gains is described on
%
% https://confluence.skatelescope.org/display/TDT/Signal+Chain+Behavioural+Modeling
%
% The data from the EM simulations have a considerable size and can be made
% available on request.
%
% Stefan J. Wijnholds, 5 October 2020
%
% Simulation tools
%   receive_path_gain           - script to generate SKA1-LOW receive path gains
%   generate_vis_diffuse_pol    - generates intra-station visibilities
%                                 based on Haslam map
%   generate_vis_pointsrc_pol   - generates intra-station visibilities
%                                 based on point source model
%   pol_EEP_SKALA4AL            - returns Jones matrices describing EEPs
%   SKA_LOW_statbeam            - predicts SKA-LOW station beam response
%
% Support functions
%   dyn_scatter_plot            - shows time and space dependent data as movie
%   gen_gLNA                    - generates gains for LNAs
%   gen_gFEM                    - generates gains for FEMs
%   gen_gFO                     - generates gains for fiber optic (FO) links
%   gen_g_preADU                - generates gains for pre-ADUs
%   h5write_all                 - stores simulation results in HDF5 format
%   JulianDay                   - converts UTC time to Julian Day number
%   precessionMatrix            - calculates precession w.r.t, J2000
%   project_radec_to_lm         - maps (ra,dec)-map to (l,m)-map
%   radectolm                   - convers (ra,dec) coords to (l,m) coords
