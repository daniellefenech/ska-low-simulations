function R = generate_vis_diffuse_pol(pos, freq, tobs, lon, lat, eep_fun)

% R = generate_vis_pointsrc_pol(pos, freq, tobs, lon, lat, eep_fun)
%
% This function computes the array covariance matrix / visibility matrix
% based on the Haslam map taking into account the embedded element patterns
% (EEPs) of the individual antennas.
%
% Arguments
% pos     : Nant-by-2 matrix of (x, y)-positions in the horizon plane of
%           the station in m
% freq    : observing frequency in Hz
% tobs    : observing time as datenum in UTC
% lon     : geographical longitude of the station site in rad
% lat     : geographical latitude of the station site in rad
% eep_fun : function handle to a function to evaluate the embedded element
%           patterns at specified (l, m)-coordinates in terms of their
%           Jones matrices
%
% Return value
% R       : (2 * Nant)-by-(2 * Nant) array covariance matrix in which
%           each 2-by-2 block represents the coherence / visibility of a
%           single intra-station baseline
%
% Stefan J. Wijnholds, 17 August 2020

% useful constant
c = 2.99792e8;      % speed of light in m/s
kB = 1.38e-23;      % Boltzmann constant in J/K
Nant = size(pos, 1);

%% Source model

% load Haslam map
load Haslam_orig.mat

% determine maximum baseline
u = meshgrid(pos(:, 1)) - meshgrid(pos(:, 1)).';
v = meshgrid(pos(:, 2)) - meshgrid(pos(:, 2)).';
uvdist = sqrt(u.^2 + v.^2);
uvmax = max(uvdist(:));

% define sampling of sky map at 5 times array resolution
lambda = c / freq;
res = lambda / uvmax;
dl = res / 5;
% project data on (l,m)-grid
l = 0:dl:1;
l = [-fliplr(l(2:end)), l];
[lgrid, mgrid] = meshgrid(l);
lmmap = project_radec_to_lm(ra, dec, intensity408MHz, l, l, lon, lat, 1, tobs);
% define list of (l,m)-pixels removing pixels too close to horizon
dist = sqrt(lgrid.^2 + mgrid.^2);
lm = [lgrid(dist(:) < 0.99), mgrid(dist(:) < 0.99)];
Nsrc = size(lm, 1);

% apply appropriate physical conversion and corrections
% convert map to units of Kelvin (at 408 MHz)
Tmap = lmmap / 10;
% convert from temperature map to intensity map (in Jy / sr) (see TMS)
lambda408 = c / 408e6;
Imap = 1e26 * (2 * kB * Tmap / lambda408^2);
% convert to flux in Jy / pixel
dOmega = dl.^2 ./ sqrt(1 - dist.^2);
dOmega(dist >= 0.99) = NaN; % remove pixels that can potentially blow up
Smap = Imap .* dOmega;
S408MHz = Smap(dist(:) < 0.99);
S = S408MHz * (lambda / lambda408).^0.55; % correction for sky noise spectrum

%% Instrument model
J = eep_fun(lm(:, 1), lm(:, 2));

%% Array covariance matrix
R = zeros(2 * Nant, 2 * Nant);
for srcidx = 1:Nsrc
    % array response matrix including element response
    % note that delay has been included in EEPs
    A = zeros(2 * Nant, 2);
    for antidx = 1:Nant
        A(2 * antidx - 1:2 * antidx, :) = J(:, :, antidx, srcidx);
    end
    % add source to visibilities
    R = R + S(srcidx) * (A * A');
end
