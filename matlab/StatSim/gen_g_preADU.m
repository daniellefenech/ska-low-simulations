function [g_preADUx, g_preADUy] = gen_g_preADU(f, epsmag_std, epsph_std, Nant)

% [g_preADUx, g_preADUy] = gen_g_preADU(f, epsmag_std, epsph_std, Nant)
%
% Function to generate gains of the pre-ADU of the SKA-LOW antennas. As the
% pre-ADUs are installed in a temperature-controlled environment, it is
% assumed that the dynamic variations of the pre-ADU gains are negligible.
%
% For more details on this model and the underlying assumptions, the reader
% is referred to
% https://confluence.skatelescope.org/display/TDT/Signal+Chain+Behavioural+Modeling
%
% Arguments:
% f          : Nf-element column vector with frequencies at which to
%              generate gain values (in Hz)
% epsmag_std : RMS gain magnitude variation between receive paths (in dB)
% epsph_std  : RMS gain phase variations between receive paths (in deg)
% Nant       : number of receive paths
%
% Return value
% g_preADUx  : Nant x Nf matrix containing generated LNA gain values
%              (complex valued, voltage domain) for x-polarisation
% g_preADUy  : Nant x Nf matrix containing generated LNA gain values
%              (complex valued, voltage domain) for y-polarisation
%
% Stefan J. Wijnholds, 3 August 2020
% Last modified on 17 August 2020 by Stefan J. Wijnholds

% determine number of frequencies
Nf = length(f);

% production tolerances
% modelled as constant offsets across frequency for each element, offsets
% assumed to have Gaussian distribution across elements
epsmag = epsmag_std * randn(2 * Nant, 1);
epsph = epsph_std * randn(2 * Nant, 1);
gerr = 10.^(epsmag/20) .* exp(1i * epsph * pi / 180);

% assume a nominal gain of unity for all frequencies
g_preADUx = repmat(gerr(1:Nant), [1, Nf]);
g_preADUy = repmat(gerr(Nant+1:end), [1, Nf]);

