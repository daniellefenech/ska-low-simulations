function h5write_all(fname, attributes, datasets)

% h5write_all(fname, attributes, datasets)
%
% Save a set of MATLAB metadata and arrays to a HDF5 file.
% Example:
%     h5write_all('data.h5', ...
%         {'num_channels', int32(40), 'num_times', int32(10)}, ...
%         {'name1', array1, 'name2', array2})
%
% Inputs:
%     fname: Name of HDF5 file to write.
%     attributes: Cell array containing (name, value) pairs of attributes.
%     datasets: Cell array containing (name, value) pairs of datasets.
%
% Fred Dulwich, 18 September 2020

% Create a new HDF5 file and get a handle to it.
handle = H5F.create(fname, 'H5F_ACC_TRUNC', ...
            H5P.create('H5P_FILE_CREATE'), H5P.create('H5P_FILE_ACCESS'));

% Iterate over attributes to write.
num_attributes = length(attributes) / 2;
for i = 0:num_attributes-1
    write_attribute(handle, attributes{2*i + 1}, attributes{2*i + 2})
end

% Iterate over datasets to write.
num_datasets = length(datasets) / 2;
for i = 0:num_datasets-1
    write_dataset(handle, datasets{2*i + 1}, datasets{2*i + 2})
end

% Close the HDF5 file.
H5F.close(handle);
end

%%
function write_attribute(handle, name, value)
% Get the HDF5 data type.
switch (class(value))
    case 'double'
        type_id = H5T.copy('H5T_IEEE_F64LE');
    case 'single'
        type_id = H5T.copy('H5T_IEEE_F32LE');
    case 'int32'
        type_id = H5T.copy('H5T_STD_I32LE');
    otherwise
        return
end

% Create and write the attribute.
attr_id = H5A.create(handle, name, type_id, ...
    H5S.create('H5S_SCALAR'), 'H5P_DEFAULT', 'H5P_DEFAULT');
H5A.write(attr_id, 'H5ML_DEFAULT', value)

% Close the attribute.
H5T.close(type_id);
H5A.close(attr_id);
end

%%
function write_dataset(handle, name, array)
% Get the HDF5 data type.
switch (class(array))
    case 'double'
        type_id = H5T.copy('H5T_IEEE_F64LE');
    case 'single'
        type_id = H5T.copy('H5T_IEEE_F32LE');
    case 'int32'
        type_id = H5T.copy('H5T_STD_I32LE');
    otherwise
        return
end

% Switch from column-major (Matlab) to row-major (C).
% array = permute(array, ndims(array):-1:1);

% Write real or complex data.
if (isreal(array))
    dataset = H5D.create(handle, name, type_id, ...
        H5S.create_simple(ndims(array), ...
        fliplr(size(array)), fliplr(size(array))), 'H5P_DEFAULT');
    H5D.write(dataset, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', ...
        'H5P_DEFAULT', array);
else
    % Create a 2-element compound type for complex numbers.
    element_size = H5T.get_size(type_id);
    element_type = H5T.create('H5T_COMPOUND', 2 * element_size);
    H5T.insert(element_type, 'r', 0, type_id);
    H5T.insert(element_type, 'i', element_size, type_id);
    dataset = H5D.create(handle, name, element_type, ...
        H5S.create_simple(ndims(array), ...
        fliplr(size(array)), fliplr(size(array))), 'H5P_DEFAULT');
    H5D.write(dataset, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', ...
        'H5P_DEFAULT', struct('r', real(array), 'i', imag(array)));
end

% Close the dataset.
H5T.close(type_id);
H5D.close(dataset);
end
