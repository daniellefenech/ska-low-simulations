#WORKDIR=./test_results

#IMAGES=nexus.engageska-portugal.pt/rascil-docker

lint:
# outputs linting.xml
	pylint --exit-zero --output-format=pylint2junit.JunitReporter scripts > linting.xml
	pylint --exit-zero --output-format=parseable scripts

test_unittest:
	HOME=`pwd` py.test \
        tests --verbose \
	--junitxml unit-tests.xml \
	--cov scripts \
	--cov-report term \
	--cov-report html:coverage  \
	--cov-report xml:coverage.xml

#everything: rm_all build_all test_all

#.phony: build_all rm_all test_all test_all test_base test_full test_notebook test_all_singularity \
#	test_all_singularity test_base_singularity test_full_singularity test_notebook_singularity everything

