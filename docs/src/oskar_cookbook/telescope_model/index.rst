.. _telescope_model:

Setting up a telescope model
****************************

An OSKAR telescope model encapuslates all static (time-invariant) data needed
to describe a telescope configuation.

Physically, a telescope model consists of a directory hierarchy which holds
the data for each station in the telescope.
Signals from stations at the root-level of the telescope model are
cross-correlated, while elements and sub-stations (in sub-directories) are
beam-formed first to generate each station beam.

It is often sufficient to set up the telescope model at the same time as the
other settings parameters simply by specifying the input directory (and any
other options), but it can sometimes be necessary to set it explicitly.

Example: Overriding element data
--------------------------------

For example, to override the some values in the model after it has been loaded:

.. code-block:: python

    params = {
        # Set up all required simulation parameters here...
        ...
        'telescope': {
            'input_directory': 'telescope.tm'
        }
    }
    settings = oskar.SettingsTree('oskar_sim_interferometer')
    settings.from_dict(params)

    # Create the telescope model from the settings parameters.
    tel = oskar.Telescope(settings=settings)

    # Override element gains.
    tel.override_element_gains(mean=1, std=0.03, seed=1)

    # Override element cable length errors.
    tel.override_element_cable_length_errors(std=0.015)

The telescope model can then be set programmatically using
``oskar.Interferometer.set_telescope_model(tel)``.
