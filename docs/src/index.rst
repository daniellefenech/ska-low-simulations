.. _documentation_master:

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

ska-low-simulations
===================

This repository collects scripts used for various SKA-Low simulations.

Direction-dependent effects
---------------------------

Many of the simulations of direction-dependent effects on the sky make
use of the `OSKAR <https://developer.skatelescope.org/projects/sim-tools/>`_
simulator. Scripts for these simulations are written specifically for each
investigation.

A "cookbook" is provided here to help descibe the examples that use OSKAR,
and to provide a starting point for writing new scripts.

.. toctree::
  :maxdepth: 2

  oskar_cookbook/index

Low-level RFI
-------------

.. toctree::
  :maxdepth: 2

  lowlevel-rfi/index


Download
========

To clone the repository, use:

  .. code-block:: bash

     git clone https://gitlab.com/ska-telescope/ska-low-simulations.git

Or browse the files at https://gitlab.com/ska-telescope/ska-low-simulations
