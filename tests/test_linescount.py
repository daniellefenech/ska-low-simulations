import unittest
import os

class TestLinescount(unittest.TestCase):

    def actualSetUp(self, filename="scripts/run_sims_beam_errors.py"):
        cwd = os.getcwd()
        print(cwd)
        self.filename = cwd + "/" + filename

    def _linesCount(self):
        print(self.filename)
        f = open(self.filename)
        number_of_lines = len(f.readlines(  ))
        f.close()
        print(self.filename, number_of_lines)

        assert number_of_lines > 0

    def test_script1(self):
        self.actualSetUp(filename="scripts/run_sims_beam_errors.py")
        self._linesCount()

    def test_script2(self):
        self.actualSetUp(filename="scripts/run_sims_ionosphere_spot_freqs.py")
        self._linesCount()

    def test_script3(self):
        self.actualSetUp(filename="scripts/run_sims_station_layouts_bp.py")
        self._linesCount()

if __name__ == '__main__':
    unittest.main()



