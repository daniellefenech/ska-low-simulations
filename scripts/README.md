## SKA1-LOW simulation scripts

| Script name   | Functionality   |
| ------------- | --------------- | 
|create_ionospheric_screen.py|Generate a test ionsopheric screen with multiple layers.|
|generate_station_layouts.py |Generate 2D random points with a minimum separation within a radius specified by r_max.|
|low_dd_effects_validation.py| |
|plot_beams.py|Generates all-sky zenith-centred beam patterns for SKALA-4 and EDA-2 antennas|
|plot_stokes.py| |
|power_spectrum_test.py| |
|run_GLEAM_EoR1_SKA1-Low_Generic_Set1.py|Generate some initial simulated datasets (SIM-147 and SIM-197)|
|run_sims_beam_errors.py|Script to run LOW station beam error simulations|
|run_sims_element_patterns.py|Script to run LOW station beam error simulations|
|run_sims_ionosphere_spot_freqs.py|Script to run LOW simulations at spot frequencies with ionosphere instead of station beam errors|
|run_sims_low_dd_effects_beams.py|Run simulations for SKA1-LOW direction-dependent effects, SP-1056|
|run_sims_low_dd_effects.py|Run simulations for SKA1-LOW direction-dependent effects, SIM-489|
|run_sims_obs_length.py|Script to run LOW station beam error simulations|
|run_sims_spot_freqs_eda2.py|Script to run LOW station beam error simulations at spot frequencies|
|run_sims_spot_freqs_skala4.py|Script to run LOW station beam error simulations at spot frequencies|
|run_sims_station_layouts_bp.py|Script to run LOW station layout simulations at spot frequencies. Generate cross-power beam patterns.|
|run_sims_station_layouts_fscn.py|Script to run LOW station layout simulations at spot frequencies. Use FSCN as a metric this time.|
|run_sims_station_layouts.py|Script to run LOW station layout simulations at spot frequencies|
|run_sims_unpolarised_grid_beam_errors.py|Run polarisation simulations with beamforming errors|
|run_sims_unpolarised_grid_element.py|Initial script to run polarisation simulations|
|run_sims_unpolarised_grid_full.py|Run polarisation simulations with beamforming|
|run_sims_wide_bandwidth.py|Script to run LOW station beam error simulations|
|utils.py|Local module with some utility finction(s)|
